/* New scripts */
$(document).ready(function() {

    $('.on-top').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
    });

    $('.mnu-tabs a').click(function( e ){
        var self = $(this),
            target = self.attr('href'),
            action = 'close';

        if( /^#/.test(target) ) {
            target = $( target );
        }

        if( target instanceof jQuery && target.length > 0 && target.hasClass('mnu-popup') ) {
            e.preventDefault();

            if( self.hasClass('active') ) {
                self.removeClass('active');
                target.css("left", "-310px").removeClass('opened');
                $(".wrapper").css({"right": "0", "left": "0", "position": "relative"});
            } else {
                self.addClass('active');
                target.css("left", "0").addClass('opened');
                $(".wrapper").css({"right": "-310px", "left": "310px", "position": "fixed"});
            }

            return;
        }

        if( $('.mnu-popup.opened').length > 0 ) {
            var openedTarget = $('.mnu-popup.opened'),
                menuTarget = $( 'a[href="#'+ openedTarget.attr('id') +'"]' );

            menuTarget.removeClass('active');
            openedTarget.css("left", "-310px").removeClass('opened');
            $(".wrapper").css({"right": "0", "left": "0", "position": "relative"});
        }
    });

    $('.mnu-tabs-mob__burg').click(function(e){
        e.preventDefault();
        if(!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(".mnu-popup").slideDown();
            $(".wrapper").css("position", "fixed");
        }
        else {
            $(this).removeClass('active');
            $(".mnu-popup").slideUp();
            $(".wrapper").css("position", "relative");
        }
    });

    $('.product-slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1
    });

    //faidIn widgets
    $('.content__inner, .product-content__info, .product-content__item, .product-list__item-info').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function () {
            $(this).addClass('visible');
            // Do something to the matched elements as they come in
        },
        doOut: function () {
            // Do something to the matched elements as they get off scren
        },
        tolerance: 20,
        throttle: 50,
        toggleClass: null,
        lazyAttr: null,
        lazyPlaceholder: 'someImage.jpg',
        debug: false
    });
    //faidIn widgets

    $('.mnu-popup > ul').perfectScrollbar();

    $('a', '.models-tabs').on('click', function(e){
        e.preventDefault();

        var self = $(this),
            offetTop = $( self.attr('href') ).offset().top - 101 - self.closest('.models-tabs').height();

        $("html, body").stop().animate({ scrollTop: offetTop }, 600);
    });

    function scrollToHash( el) {
        var target = el || $( window.location.hash ), scrollUpAcc;

        if( target.length > 0 ) {
            scrollUpAcc = target.offset().top -101;
            $("html, body").stop().animate({ scrollTop: scrollUpAcc }, 600);
        }
    }

    if( window.location.hash !== '' ) {
        if( $('img').length > 0 ) {
            $('img').on('load', function() {
                scrollToHash( $(window.location.hash) );
            });
        } else {
            scrollToHash( $(window.location.hash) );
        }
    }

    $('a', '.footer-tabs, .mnu-tabs').on('click', function( e ) {
        if( e.target.hash !== '' && $(e.target.hash).length > 0 ) {
            scrollToHash( $(e.target.hash) );
        }
    });
    /*
     $('.main-page .mnu-tabs a').click(function(e){
     e.preventDefault();
     if($(this).hasClass('mnu-tabs__gallery')) {
     var item = $('.main-instagram');
     var scrollUpAcc = item.offset().top -101;
     $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
     }

     else if($(this).hasClass('mnu-tabs__about')) {
     var item = $('.main-info-block');
     var scrollUpAcc = item.offset().top -101;
     $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
     };
     });*/

    var videoHeightForHeader = $('.main-story').height() / 2;

    $(window).scroll(function(){
        var topOfHeader = $('.header').offset().top;
        if(topOfHeader >= videoHeightForHeader) {
            $('.header').css("background-color", "#1F2123");
        }
        else {
            $('.header').css("background-color", "transparent");
        }
    });


    $('.more-insta')
        .data('counter', 0)
        .click(function(e) {
            var counter = $(this).data('counter');
            if(counter <= 2) {
                e.preventDefault();
            }
            $(this).data('counter', counter + 1);
            console.log($(this).data('counter'));
        });

    // $(window).load(function(){
    //     var linkEl = $('.mnu-tabs li a');
    //     var landingItem = $('.content > div');
    //     $('.mnu-tabs').liLanding({
    //         show: function (linkEl, landingItem) {
    //         },
    //         hide: function (linkEl, landingItem) {
    //         }
    //     });
    // });

    //faidIn widgets
    $('.js-scroll').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function () {

            var target = $(this).attr('id');
            $('.js-scroll-nav-item').removeClass('active');
            $('.js-scroll-nav-item[href="#' + target + '"]').addClass('active');


            // Do something to the matched elements as they come in
        },
        doOut: function () {
            // Do something to the matched elements as they get off scren
        },
        tolerance: 400,
        throttle: 50,
        toggleClass: null,
        lazyAttr: null,
        lazyPlaceholder: 'someImage.jpg',
        debug: false
    });
    //faidIn widgets

    $('[data-action]').on('click', function( e ) {
        e.preventDefault();

        var self = $(this),
            action = self.data('action');

        $('body').trigger('action-'+ action, [ self ]);
    });

    $('body').on('action-order', function( e, target ){
        e.preventDefault();

        var showcase = target.data('showcase'),
            popup = $( '[data-order-form]' );

        $('[name="bike-showcase"]', popup).val( showcase );

        popup.trigger( 'open' );

    }).on( 'applyFilter', function( e, filterConteiner, filterBy, filterVal ) {
        e.preventDefault();

        if( 'reset' == filterBy ) {
            $('[data-filter-by]', filterConteiner).slideDown('fast').fadeIn('fast');

            return;
        }
        $('[data-filter-by="'+ filterBy +'"]', filterConteiner).not( '[data-filter-value="'+ filterVal +'"]' ).slideUp('fast').fadeOut('fast');

    });


    $('.close-popup').on( 'click', function( e ){
        e.preventDefault();

        var self = $(this),
            conteiner = self.closest( '[data-modal]' );

        conteiner.trigger('close');
    });

    $('[data-modal]').on('close', function( e ) {
        e.preventDefault();

        var self = $(this);

        self.fadeOut(200, function() {
            $('.wpcf7-response-output, .wpcf7-not-valid-tip', self).remove();
            $('.wpcf7-not-valid', self).removeClass('wpcf7-not-valid');
        });
    }).on('open', function( e ) {
        e.preventDefault();

        var self = $(this);

        self.hide().removeClass('hidden').fadeIn(200);
    });

    // Filter
    $('[data-filter]').on( 'click', function( e ) {
        var self = $(this),
            filterBy = self.attr('data-filter'),
            filterVal = self.attr('data-filter-value'),
            conteiner = self.attr('data-filter-conteiner');

        $('body').trigger('applyFilter', [conteiner, filterBy, filterVal ]);
    });

    if( '' !== location.hash && $('[data-filter="'+ location.hash.substr(1) +'"]').length > 0 ) {
        var filterEl = $('[data-filter="'+ location.hash.substr(1) +'"]'),
            filterBy = filterEl.attr('data-filter'),
            filterVal = filterEl.attr('data-filter-value'),
            conteiner = filterEl.attr('data-filter-conteiner');

        $('body').trigger('applyFilter', [conteiner, filterBy, filterVal ]);
    }

    $(window).on('load', function () {
        var $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('.spinner');

        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow');
    });
});
/* New scripts */


/**
 * Created by Valera on 21.07.16.
 */
$(document).ready(function() {

     $('.on-top').click(function(){
         $("html, body").animate({ scrollTop: 0 }, 600);
     });

    $('.mnu-tabs li a, .footer-tabs li a').click(function(e){
        if($(this).hasClass('mnu-tabs__bikes') || $(this).hasClass('footer-tabs__bikes')) {
            e.preventDefault();
            if (!$(this).hasClass('active')) {
                $('.mnu-tabs__bikes, .footer-tabs__bikes').addClass('active');
                $(".mnu-popup").css("left", "0");
                $(".wrapper").css({"right": "-310px", "left": "310px", "position": "fixed"});
            }
            else {
                $('.mnu-tabs__bikes, .footer-tabs__bikes').removeClass('active');
                $(".mnu-popup").css("left", "-310px");
                $(".wrapper").css({"right": "0", "left": "0", "position": "relative"});
            }
        }
        else {
            $('.mnu-tabs__bikes, .footer-tabs__bikes').removeClass('active');
            $(".mnu-popup").css("left", "-310px");
            $(".wrapper").css({"right": "0", "left": "0", "position": "relative"});
        }
    });

    $('.mnu-tabs-mob__burg').click(function(e){
        e.preventDefault();
        if(!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(".mnu-popup").slideDown();
            $(".wrapper").css("position", "fixed");
        }
        else {
            $(this).removeClass('active');
            $(".mnu-popup").slideUp();
            $(".wrapper").css("position", "relative");
        }
    });

    $('.product-slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1
    });

    $('.close-popup').click(function(){
        $('.popup-bg').fadeOut(300);
        $('.popup').fadeOut(300);
        $('.popup-thank').fadeOut(300);
    });

    //faidIn widgets
    $('.content__inner, .product-content__info, .product-content__item, .product-list__item-info').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function () {
            $(this).addClass('visible');
            // Do something to the matched elements as they come in
        },
        doOut: function () {
            // Do something to the matched elements as they get off scren
        },
        tolerance: 20,
        throttle: 50,
        toggleClass: null,
        lazyAttr: null,
        lazyPlaceholder: 'someImage.jpg',
        debug: false
    });
    //faidIn widgets

    $('.mnu-popup > ul').perfectScrollbar();

    $('.models-tabs a').click(function(e){
       e.preventDefault();
        if($(this).hasClass('models-tabs__digger')) {
            var item = $('.product-list__item-digger');
            var scrollUpAcc = item.offset().top - 101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        }
        else if($(this).hasClass('models-tabs__rocketeer')) {
            var item = $('.product-list__item-rocketeer');
            var scrollUpAcc = item.offset().top - 101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        }
        else if($(this).hasClass('models-tabs__browler-gt')) {
            var item = $('.product-list__item-browler-gt');
            var scrollUpAcc = item.offset().top - 101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        }
        else if($(this).hasClass('models-tabs__69-chopper')) {
            var item = $('.product-list__item-69-chopper');
            var scrollUpAcc = item.offset().top - 101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        }
        else if($(this).hasClass('models-tabs__rocketeer-f3')) {
            var item = $('.product-list__item-rocketeer-3');
            var scrollUpAcc = item.offset().top - 101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        };
    });

    $('.main-page .mnu-tabs a, .main-page .footer-tabs a').click(function(e){

        if($(this).hasClass('mnu-tabs__gallery') || $(this).hasClass('footer-tabs__gallery')) {
            e.preventDefault();
            var item = $('.main-instagram');
            var scrollUpAcc = item.offset().top -101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        }

        else if($(this).hasClass('mnu-tabs__about') || $(this).hasClass('footer-tabs__about')) {
            e.preventDefault();
            var item = $('.main-info-block');
            var scrollUpAcc = item.offset().top -101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        }

        else if($(this).hasClass('mnu-tabs__parts') || $(this).hasClass('footer-tabs__parts')) {
            e.preventDefault();
            var item = $('.main-parts');
            var scrollUpAcc = item.offset().top -101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        };
    });
    
    var videoHeightForHeader = $('.main-story').height() / 2;

    $(window).scroll(function(){
        var topOfHeader = $('.header').offset().top;
        if(topOfHeader >= videoHeightForHeader) {
            $('.header').css("background-color", "#1F2123");
        }
        else {
            $('.header').css("background-color", "transparent");
        }
    });
    

    $('.more-insta')
        .data('counter', 0)
        .click(function(e) {
            var counter = $(this).data('counter');
            if(counter <= 2) {
                e.preventDefault();
            }
            $(this).data('counter', counter + 1);
            console.log($(this).data('counter'));
        });

    // $(window).load(function(){
    //     var linkEl = $('.mnu-tabs li a');
    //     var landingItem = $('.content > div');
    //     $('.mnu-tabs').liLanding({
    //         show: function (linkEl, landingItem) {
    //         },
    //         hide: function (linkEl, landingItem) {
    //         }
    //     });
    // });

    //faidIn widgets
    $('.js-scroll').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function () {

            var target = $(this).attr('id');
            $('.js-scroll-nav-item').removeClass('active');
            $('.js-scroll-nav-item[href="#' + target + '"]').addClass('active');


            // Do something to the matched elements as they come in
        },
        doOut: function () {
            // Do something to the matched elements as they get off scren
        },
        tolerance: 400,
        throttle: 50,
        toggleClass: null,
        lazyAttr: null,
        lazyPlaceholder: 'someImage.jpg',
        debug: false
    });
    //faidIn widgets

    $('.mnu-icons__icon-share').click(function(e){
        var clicked = $(this);
        e.preventDefault();
       if(!clicked.parent().hasClass('active')) {
           clicked.parent().addClass('active');
           $('.mnu-icons__share-list').slideDown(300);

           $(document).on("click touchstart", function (e){
               var popUp = $(".mnu-icons__share-list");
               var button = clicked.parent();
               if (!popUp.is(e.target)
                   && popUp.has(e.target).length === 0
                   && !button.is(e.target)
                   && button.has(e.target).length === 0
                   && button.hasClass('active')) {
                   setTimeout(function(){
                       button.removeClass('active');
                   }, 300);

                   popUp.slideUp(300);
               }

           });
       }
        else {
           setTimeout(function(){
               clicked.parent().removeClass('active');
           }, 300);

           $('.mnu-icons__share-list').slideUp(300);
       }
    });

    $('.product-content__share-btn').click(function(e){
        var clicked = $(this);
        var socList = clicked.siblings('.product-content__share-list');
        var socIcons = socList.find('a');
        var windowWidth = $(window).width();
        e.preventDefault();
        if(!clicked.hasClass('active')) {
            clicked.addClass('active');
            if(windowWidth > 932) {
                socList.css('display', 'block');
                setTimeout(function(){
                socList.css('width', '350px');
                }, 100);
            }
            else {
                socList.css('display', 'block');
                setTimeout(function(){
                socList.slideDown(300);
                }, 100);
            }
            setTimeout(function(){
                socIcons.css('opacity', '1');
            }, 400);


            $(document).on("click touchstart", function (e){
                if (!socList.is(e.target)
                    && socList.has(e.target).length === 0
                    && !clicked.is(e.target)
                    && clicked.has(e.target).length === 0
                    && clicked.hasClass('active')) {
                    setTimeout(function(){
                        if(windowWidth > 932) {
                            socList.css('width', '0');
                            setTimeout(function(){
                                socList.css('display', 'none');
                            }, 500);
                        }
                        else {
                            socList.slideUp(300);
                            setTimeout(function(){
                                socList.css('display', 'none');
                            }, 400);
                        }
                    setTimeout(function(){
                        clicked.removeClass('active');
                    }, 300);
                    }, 100);
                    socIcons.css('opacity', '0');
                }

            });
        }
        else {
            setTimeout(function(){
                if(windowWidth > 932) {
                    socList.css('width', '0');
                    setTimeout(function(){
                        socList.css('display', 'none');
                    }, 500);
                }
                else {
                    socList.slideUp(300);
                    setTimeout(function(){
                        socList.css('display', 'none');
                    }, 400);
                }
                setTimeout(function(){
                    clicked.removeClass('active');
                }, 300);
            }, 100);
            socIcons.css('opacity', '0');
        }
    });

    var zIndex = $('.product-content__item').eq(0).zIndex();
    $('.product-content__item').each(function(index){
        var el = $(this);
        el.css('zIndex', zIndex);
        zIndex -= 200;
    });
});

$(window).on('load', function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});