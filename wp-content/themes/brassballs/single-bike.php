<?php get_header(); ?>
<?php if( have_posts() ) { ?>
    <?php while ( have_posts() ) { the_post(); ?>
        <div class="product-title">
            <a href="<?php echo get_post_type_archive_link('bike'); ?>" class="product-title__back">Back</a>
            <?php the_title('<h1>', '</h1>'); ?>
            <?php 
            if( have_rows('page_cover') ) {
                while( have_rows('page_cover') ) { the_row();
            ?>
            <div class="product-title__img parallax-window" data-parallax="scroll" data-image-src="<?php the_sub_field('cover_image'); ?>">
                <h2><?php the_sub_field('cover_text'); ?></h2>
            </div>
            <?php
                }
            } ?>
        </div>
        <div class="product-content">
            <div class="product-content__info">
                <?php the_content(); ?>
            </div>
            <?php get_template_part('templates/bike', 'showcases' ); ?>
        </div>
        <?php get_template_part('templates/bike', 'gallery' ); ?>
        <?php bballs_bikes_each( 'bballs_bike_slider', '<div class="product-slider">', '</div>' ); ?>
    <?php } ?>
<?php }

get_template_part('templates/popup', 'order' );

get_footer('single-bike');