<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div id="page-preloader"><span class="spinner"></span></div> 
    <div class="wrapper">
        <div class="header">
            <?php
                wp_nav_menu([
                    'theme_location' => 'primary',
                    'menu_class' => 'mnu-tabs',
                    'container' => false,
                    'items_wrap' => '<ul class="%2$s">%3$s</ul>'
                ]);
            ?>
            <?php /*
            <ul class="mnu-tabs">
                <li><a class="mnu-tabs__bikes" href="#">Bikes</a></li>
                <li><a class="js-scroll-nav-item" href="#parts">Parts</a></li>
                <li><a class="mnu-tabs__gallery js-scroll-nav-item" href="#gallery">Gallery</a></li>
                <li><a class="mnu-tabs__about js-scroll-nav-item" href="#about">About</a></li>
            </ul>
            */?>
            <ul class="mnu-tabs-mob">
                <li><a class="mnu-tabs-mob__burg" href="#"></a></li>
                <?php /*<li><a class="mnu-tabs-mob__search" href="#"></a></li> */?>
            </ul>
            <a href="<?php echo home_url(); ?>" class="mnu-logo"><img src="<?php echo BBALLS_THEME_URL; ?>/assets/img/header-logo.svg" alt="alt"></a>
            <ul class="mnu-icons">
                <?php /*<li><a href="#" class="mnu-icons__icon-search"></a></li> 
                <li><a href="#" class="mnu-icons__icon-cart"></a></li> */ ?>
                <li><a href="#" class="mnu-icons__icon-share"></a></li> 
            </ul>
        </div>
        <div class="mnu-popup" id="side-bikes">
            <?php get_template_part( 'templates/side-bikes' ); ?>
        </div>
        <?php if ( is_archive('bike') ) {
            bballs_bikes_each( 'bballs_bakes_tabs', '<ul class="models-tabs">', '</ul>' );
        } ?>
        <div class="content">