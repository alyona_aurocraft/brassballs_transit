<?php
/**
 * Template Name: Ready to buy
 */
get_header();

query_posts( [
    'post_type' => 'bike',
    'posts_per_page' => -1,
    'meta_query' => [
        [
            'key' => 'ready_to_buy',
            'value' => '1',
            'compare' => '=',
        ]
    ]
]);

if( have_posts() ) {
?>
    <?php get_template_part('templates/bike', 'filter' ); ?>
    <ul class="product-list">
    <?php while ( have_posts() ) { the_post(); ?>
        <li class="product-item">
            <div class="product-list__item-info">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="product-content">
                <?php get_template_part('templates/bike', 'showcases' ); ?>
            </div>
        </li>
    <?php } ?>
    </ul>
<?php }

wp_reset_query();

get_template_part('templates/popup', 'order' );

get_footer();