/**
 * Created by Valera on 21.07.16.
 */
$(document).ready(function() {

     $('.on-top').click(function(){
         $("html, body").animate({ scrollTop: 0 }, 600);
     });

    $('.mnu-tabs a').click(function( e ){
        var self = $(this),
            target = self.attr('href'),
            action = 'close';

        if( /^#/.test(target) ) {
            target = $( target );
        }

        if( target instanceof jQuery && target.length > 0 && target.hasClass('mnu-popup') ) {
            e.preventDefault();

            if( self.hasClass('active') ) {
                self.removeClass('active');
                target.css("left", "-310px").removeClass('opened');
                $(".wrapper").css({"right": "0", "left": "0", "position": "relative"});
            } else {
                self.addClass('active');
                target.css("left", "0").addClass('opened');
                $(".wrapper").css({"right": "-310px", "left": "310px", "position": "fixed"});
            }

            return;
        }

        if( $('.mnu-popup.opened').length > 0 ) {
            var openedTarget = $('.mnu-popup.opened'),
                menuTarget = $( 'a[href="#'+ openedTarget.attr('id') +'"]' );

            menuTarget.removeClass('active');
            openedTarget.css("left", "-310px").removeClass('opened');
            $(".wrapper").css({"right": "0", "left": "0", "position": "relative"});
        }
    });

    $('.mnu-tabs-mob__burg').click(function(e){
        e.preventDefault();
        if(!$(this).hasClass('active')) {
            $(this).addClass('active');
            $(".mnu-popup").slideDown();
            $(".wrapper").css("position", "fixed");
        }
        else {
            $(this).removeClass('active');
            $(".mnu-popup").slideUp();
            $(".wrapper").css("position", "relative");
        }
    });

    $('.product-slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1
    });

    //faidIn widgets
    $('.content__inner, .product-content__info, .product-content__item, .product-list__item-info').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function () {
            $(this).addClass('visible');
            // Do something to the matched elements as they come in
        },
        doOut: function () {
            // Do something to the matched elements as they get off scren
        },
        tolerance: 20,
        throttle: 50,
        toggleClass: null,
        lazyAttr: null,
        lazyPlaceholder: 'someImage.jpg',
        debug: false
    });
    //faidIn widgets

    $('.mnu-popup > ul').perfectScrollbar();

    $('a', '.models-tabs').on('click', function(e){
        e.preventDefault();

        var self = $(this),
            offetTop = $( self.attr('href') ).offset().top - 101 - self.closest('.models-tabs').height();

        $("html, body").stop().animate({ scrollTop: offetTop }, 600);
    });

    function scrollToHash( el) {
        var target = el || $( window.location.hash ), scrollUpAcc;

        if( target.length > 0 ) {
            scrollUpAcc = target.offset().top -101;
            $("html, body").stop().animate({ scrollTop: scrollUpAcc }, 600);
        }
    }

    if( window.location.hash !== '' ) {
        if( $('img').length > 0 ) {
            $('img').on('load', function() {
                scrollToHash( $(window.location.hash) );
            });
        } else {
            scrollToHash( $(window.location.hash) );
        }
    }

    $('a', '.footer-tabs, .mnu-tabs').on('click', function( e ) {
        if( e.target.hash !== '' && $(e.target.hash).length > 0 ) {
            scrollToHash( $(e.target.hash) );
        }
    });
/*
    $('.main-page .mnu-tabs a').click(function(e){
        e.preventDefault();
        if($(this).hasClass('mnu-tabs__gallery')) {
            var item = $('.main-instagram');
            var scrollUpAcc = item.offset().top -101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        }

        else if($(this).hasClass('mnu-tabs__about')) {
            var item = $('.main-info-block');
            var scrollUpAcc = item.offset().top -101;
            $("html, body").animate({ scrollTop: scrollUpAcc }, 600);
        };
    });*/
    
    var videoHeightForHeader = $('.main-story').height() / 2;

    $(window).scroll(function(){
        var topOfHeader = $('.header').offset().top;
        if(topOfHeader >= videoHeightForHeader) {
            $('.header').css("background-color", "#1F2123");
        }
        else {
            $('.header').css("background-color", "transparent");
        }
    });
    

    $('.more-insta')
        .data('counter', 0)
        .click(function(e) {
            var counter = $(this).data('counter');
            if(counter <= 2) {
                e.preventDefault();
            }
            $(this).data('counter', counter + 1);
            console.log($(this).data('counter'));
        });

    // $(window).load(function(){
    //     var linkEl = $('.mnu-tabs li a');
    //     var landingItem = $('.content > div');
    //     $('.mnu-tabs').liLanding({
    //         show: function (linkEl, landingItem) {
    //         },
    //         hide: function (linkEl, landingItem) {
    //         }
    //     });
    // });

    //faidIn widgets
    $('.js-scroll').onScreen({
        container: window,
        direction: 'vertical',
        doIn: function () {

            var target = $(this).attr('id');
            $('.js-scroll-nav-item').removeClass('active');
            $('.js-scroll-nav-item[href="#' + target + '"]').addClass('active');


            // Do something to the matched elements as they come in
        },
        doOut: function () {
            // Do something to the matched elements as they get off scren
        },
        tolerance: 400,
        throttle: 50,
        toggleClass: null,
        lazyAttr: null,
        lazyPlaceholder: 'someImage.jpg',
        debug: false
    });
    //faidIn widgets

    $('[data-action]').on('click', function( e ) {
        e.preventDefault();

        var self = $(this),
            action = self.data('action');

        $('body').trigger('action-'+ action, [ self ]);
    });

    $('body').on('action-order', function( e, target ){
        e.preventDefault();

        var showcase = target.data('showcase'),
            bikeName = target.data('bike-name')
            popup = $( '[data-order-form]' );

        $('[name="bike-showcase"]', popup).val( showcase );
        $('[name="bike-name"]', popup).val( bikeName );

        popup.trigger( 'open' );


    }).on( 'applyFilter', function( e, filterConteiner, filterBy, filterVal ) {
        e.preventDefault();

        if( 'reset' == filterBy ) {
            $('[data-filter-by]', filterConteiner).slideDown('fast').fadeIn('fast');

            return;
        }
        $('[data-filter-by="'+ filterBy +'"]', filterConteiner).not( '[data-filter-value="'+ filterVal +'"]' ).slideUp('fast').fadeOut('fast');
        
    });


    $('.close-popup').on( 'click', function( e ){
        e.preventDefault();

        var self = $(this),
            conteiner = self.closest( '[data-modal]' );

        conteiner.trigger('close');
    });

    $('[data-modal]').on('close', function( e ) {
        e.preventDefault();

        var self = $(this);

        self.fadeOut(200, function() {
            $('.wpcf7-response-output, .wpcf7-not-valid-tip', self).remove();
            $('.wpcf7-not-valid', self).removeClass('wpcf7-not-valid');
        });
    }).on('open', function( e ) {
        e.preventDefault();

        var self = $(this);

        self.hide().removeClass('hidden').fadeIn(200);
    });

    // Filter
    $('[data-filter]').on( 'click', function( e ) {
        var self = $(this),
            filterBy = self.attr('data-filter'),
            filterVal = self.attr('data-filter-value'),
            conteiner = self.attr('data-filter-conteiner');

        $('body').trigger('applyFilter', [conteiner, filterBy, filterVal ]);
    });

    if( '' !== location.hash && $('[data-filter="'+ location.hash.substr(1) +'"]').length > 0 ) {
        var filterEl = $('[data-filter="'+ location.hash.substr(1) +'"]'),
            filterBy = filterEl.attr('data-filter'),
            filterVal = filterEl.attr('data-filter-value'),
            conteiner = filterEl.attr('data-filter-conteiner');

        $('body').trigger('applyFilter', [conteiner, filterBy, filterVal ]);
    }

    $(window).on('load', function () { 
        var $preloader = $('#page-preloader'), 
            $spinner   = $preloader.find('.spinner'); 
            
        $spinner.fadeOut(); 
        $preloader.delay(350).fadeOut('slow'); 
    });

    // Custom validation
    $('div.wpcf7 > form').on('click', '[type="submit"]', function( e ) {
        var valid = true,
            form = $(this).closest('form');

        $(':input[aria-required]', form).each(function(){
            if( $(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio' ) {
                if( !$(this).is(':checked') ) {
                    form = false;
                }
            } else {
                if( $(this).val().length < 2 ) {
                    form = false;
                }
            }
        });

        return valid;
    });
});