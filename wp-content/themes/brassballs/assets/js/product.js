$(window).load(function() {
    if ($(window).width() > 991) {
    $('.single-project-page').perfectScrollbar();

    initSlider();

    if ($(window).width() < 992) {
        $('.carousel-track').height($('.carousel-track .carousel-slide img').height());
    }

    $('.js-img-slide').click(function () {
        if ($(this).hasClass('next'))
            nextSlide();
        if ($(this).hasClass('prev'))
            prevSlide();


    });

    $('.prev-first').click(function () {

        $('.carousel-slide.first').removeClass('active');
        $('.js-thumb.first').removeClass('active');
        $('.carousel-slide').addClass('prev').removeClass('hidden').removeClass('next');
        $('.carousel-slide.last').removeClass('prev').addClass('active');
        activeSlide = $('.carousel-slide.last').attr('data-slide');
        moveSlides();

    });

    $('.next-last').click(function () {

        $('.carousel-slide.last').removeClass('active');
        $('.js-thumb.last').removeClass('active');
        $('.carousel-slide').addClass('next').removeClass('hidden').removeClass('prev');
        $('.carousel-slide.first').removeClass('next').addClass('active');
        activeSlide = $('.carousel-slide.first').attr('data-slide');
        moveSlides();

    });

    $('.js-thumb').click(function () {
        activeSlide = $(this).data('slide');

        $('.js-img-slide.active').removeClass('active');
        $('.js-img-slide.hidden').removeClass('hidden');
        $('.js-img-slide.next').removeClass('next');
        $('.js-thumb.active').removeClass('active');
        $('.js-img-slide.prev').removeClass('prev');

        slides.each(function (i, el) {
            if (i + 1 < activeSlide) $(el).addClass('prev');
            if (i + 1 == activeSlide) $(el).addClass('active');
            if (i + 1 == activeSlide + 1) $(el).addClass('next');
            if (i + 1 > activeSlide + 1) $(el).addClass('hidden');
        });

        $('.js-thumb[data-slide="' + activeSlide + '"]').addClass('active');
        moveSlides();
    });
}
});

$(window).resize(function() {
    if ($(window).width() > 991) {
        $('.single-project-page').perfectScrollbar('destroy');
        setTimeout(function () {
            $('.single-project-page').perfectScrollbar();
        }, 300);
        $('.slide-prev').width($('.js-gallery-img.current').width());
        if ($(window).width() < 992) {
            $('.carousel-track').height($('.carousel-track .carousel-slide img').height());
        } else {
            $('.carousel-track').css({'height': '80vh'});
        }
    }
});
if ($(window).width() > 991) {
    var slides = 0,
        thumbs = 0,
        slidesCount = 0,
        activeSlide = 1,
        activeTrans = 'none',
        nextTrans = 'none',
        hiddenTrans = 'none',
        prevTrans = 'none';

// call it to initialize
    function initSlider() {
        slides = $('.js-img-slide');
        thumbs = $('.js-thumb');
        slidesCount = slides.length;

        slides.each(function (i, el) {
            $(el).attr('data-slide', i + 1);
            $(el).attr('data-index', slidesCount - i);
            $(el).attr('data-width', $(this).width());
            $(el).css({'z-index': $(this).data('index')});
        });

        thumbs.each(function (i, el) {
            $(el).attr('data-slide', i + 1);
        });

        $('.js-img-slide:first, .js-thumb:first').addClass('active first');
        $('.js-img-slide[data-slide="2"]').addClass('next');
        $('.js-img-slide:last').addClass('active last');
        $('.js-thumb:last').addClass('last');
        $('.js-img-slide:not(.first):not(.next)').addClass('hidden');

        activeTrans = (($(window).width() / 2) - ($('.js-img-slide.active').data('width') / 2)) + 'px';
        ;
        nextTrans = $('.js-img-slide.active').data('width');
        hiddenTrans = $('.js-img-slide.active').data('width') + $('.js-img-slide.next').data('width');
        prevTrans = 0;

        $('.js-img-slide.active').css({'left': activeTrans});
        $('.js-img-slide.next').css({'left': nextTrans});
        $('.js-img-slide.hidden').css({'left': hiddenTrans});
        $('.js-img-slide.prev').css({'left': prevTrans});
    }

// call it to move slides on track
    function moveSlides() {
        activeTrans = (($(window).width() / 2) - ($('.js-img-slide.active').data('width') / 2)) + 'px';
        nextTrans = (($(window).width() / 2) + ($('.js-img-slide.active').data('width') / 2)) + 'px';
        hiddenTrans = (($(window).width() / 2) - ($('.js-img-slide.active').data('width') / 2) + ($('.js-img-slide.next').data('width'))) + 'px';

        // if (activeSlide == slidesCount) activeTrans = $(window).width() - $('.js-img-slide.active').width();
        // if (activeSlide == 1) {
        //     nextTrans = $('.js-img-slide.active').data('width');
        //     hiddenTrans = $('.js-img-slide.active').data('width')+$('.js-img-slide.next').data('width');
        // }

        $('.js-img-slide.active').css({'left': activeTrans});
        $('.js-img-slide.next').css({'left': nextTrans});
        $('.js-img-slide.prev').css({'left': '0'});
        $('.js-img-slide.hidden').css({'left': hiddenTrans});

    }

// call it before move
    function renameSlides() {
        $('.js-img-slide.active').removeClass('active');
        $('.js-img-slide.hidden').removeClass('hidden');
        $('.js-img-slide.next').removeClass('next');
        $('.js-thumb.active').removeClass('active');
        // $('.js-img-slide.prev').removeClass('prev');
        $('.js-thumb[data-slide="' + activeSlide + '"]').addClass('active');
        $('.js-img-slide[data-slide="' + activeSlide + '"]').removeClass('prev');
        $('.js-img-slide[data-slide="' + activeSlide + '"]').addClass('active');
        $('.js-img-slide[data-slide="' + (activeSlide + 1) + '"]').addClass('next');
        $('.js-img-slide[data-slide="' + (activeSlide - 1) + '"]').addClass('prev');
        $('.js-img-slide:not(.active):not(.next):not(.prev)').addClass('hidden');
        // if($('.js-img-slide.last').hasClass('active')) {
        //     $('.js-img-slide.first').removeClass('prev');
        //     $('.js-img-slide.first').addClass('next');
        // }


    }

// move to next slide
    function nextSlide() {
        if (activeSlide < slidesCount)
            activeSlide++;

        renameSlides();
        moveSlides();
    }

// move to prev slide
    function prevSlide() {
        if (activeSlide > 1)
            activeSlide--;

        renameSlides();
        moveSlides();
    }

    var animations = false;

    $('.single-project-page').on('scroll', function () {
        if ($('.single-project-page').scrollTop() > $(window).height() - 200 && !animations) {
            animations = true;

            setTimeout(function () {
                animateOnce('.cost', '31', 'cost');
                setTimeout(function () {
                    $('.cost').parent().parent().addClass('showtext');
                }, 350);
            }, 100);
            setTimeout(function () {
                animateOnce('.duration', '29', 'dur');
                setTimeout(function () {
                    $('.duration').parent().parent().addClass('showtext');
                }, 350);
            }, 500);
            setTimeout(function () {
                animateOnce('.location', '35', 'loc');
                setTimeout(function () {
                    $('.location').parent().parent().addClass('showtext');
                }, 350);
            }, 900);
            setTimeout(function () {
                animateOnce('.object', '29', 'obj');
                setTimeout(function () {
                    $('.object').parent().parent().addClass('showtext');
                }, 350);
            }, 1300);
            setTimeout(function () {
                animateOnce('.process', '21', 'proc');
                setTimeout(function () {
                    $('.process').parent().parent().addClass('showtext');
                }, 350);
            }, 1700);
            setTimeout(function () {
                animateOnce('.square', '22', 'sq');
                setTimeout(function () {
                    $('.square').parent().parent().addClass('showtext');
                }, 350);
            }, 2100);
            setTimeout(function () {
                animateOnce('.roomstyle', '30', 'style');
                setTimeout(function () {
                    $('.roomstyle').parent().parent().addClass('showtext');
                }, 350);
            }, 2500);
            setTimeout(function () {
                animateOnce('.tennets', '29', 'ten');
                setTimeout(function () {
                    $('.tennets').parent().parent().addClass('showtext');
                }, 350);
            }, 2900);
        }
    });

    var swipe = false,
        swipeStart = 0,
        swipeEnd = 0;
    $('.carousel-track').on('touchmove', function (event) {
        var thisTouch = event.originalEvent.changedTouches[0];
        if (!swipe) {
            swipe = true;

            swipeStart = thisTouch.clientX;
        }
    });
    $('.carousel-track').on('touchend', function (event) {
        var thisTouch = event.originalEvent.changedTouches[0];

        swipeEnd = thisTouch.clientX;

        if (swipeEnd < swipeStart && Math.abs(swipeEnd - swipeStart) > 100) nextSlide();
        if (swipeEnd > swipeStart && Math.abs(swipeEnd - swipeStart) > 100) prevSlide();

        swipe = false;
    });
}

$(document).ready(function(){
    if ($(window).width() < 992) {

        $('.carousel-track-mob').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.carousel-thumbs-mob'
        });
        $('.carousel-thumbs-mob').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.carousel-track-mob',
            arrows: false,
            dots: false,
            centerMode: true,
            focusOnSelect: true
        });
    }
});