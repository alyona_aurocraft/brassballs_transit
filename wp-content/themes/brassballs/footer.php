            <span class="on-top">Back on top</span>
        </div>
        <div class="footer">
            <?php
                wp_nav_menu([
                    'theme_location' => 'secondary',
                    'menu_class' => 'footer-tabs',
                    'container' => false,
                    'items_wrap' => '<ul class="%2$s">%3$s</ul>'
                ]);

                wp_nav_menu([
                    'theme_location' => 'social',
                    'menu_class' => 'footer-soc-icons',
                    'container' => false,
                    'items_wrap' => '<ul class="%2$s">%3$s</ul>'
                ]);
            ?>
             <!--<ul class="footer-tabs">
                 <li><a href="#">Bikes</a></li>
                 <li><a href="#">Parts</a></li>
                 <li><a href="#">Gallery</a></li>
                 <li><a href="#">About</a></li>
                 <li><a href="#">Contact</a></li>
                 <li><a href="#">Dealers</a></li>
                 <li><a href="#">Privacy</a></li>
             </ul> 
             <ul class="footer-soc-icons">
                 <li><a class="footer-soc-icons__fb" href="#"></a></li>
                 <li><a class="footer-soc-icons__twit" href="#"></a></li>
                 <li><a class="footer-soc-icons__insta" href="#"></a></li>
             </ul>-->
         </div>
    </div>
<?php wp_footer(); ?>
</body>
</html>