<?php if( have_rows( 'showcase' ) ) { ?>
    <ul class="product-content__list">
        <?php while( have_rows( 'showcase' ) ) { the_row();
            $showcase_images = get_sub_field('images');
            $showcase_name = get_sub_field('name');
        ?>
        <li class="product-content__item">
            <?php if( !empty( $showcase_images ) ) { ?>
            <?php if( !empty( $showcase_name ) && !is_singular('bike') ) { ?>
                <h3 class="product-content__item-group-name"><?php echo $showcase_name; ?></h3>
            <?php } ?>
            <div class="product-content__item-imgs">
                <?php foreach ($showcase_images as $img_index => $img ) {
                    $img_class_sufix = 'small';
                    $img_size = 'design_small';
                    if ( 0 === $img_index ) {
                        $img_class_sufix = 'big';
                        $img_size = 'design_large';
                    }

                    $img_src = isset( $img['sizes'][ $img_size ] ) ? $img['sizes'][ $img_size ] : $img['url'] ;
                    $img_width = isset( $img['sizes'][ $img_size .'-width' ] ) ? ' width="'. $img['sizes'][ $img_size .'-width' ] .'"' : '';
                    echo '<img class="product-content__item-img-'. $img_class_sufix .'" src="'. $img_src .'" alt="alt"'. $img_width .'>';
                } ?>
            </div>
            <?php } ?>
            <div class="product-content__item-btns">
                <div class="product-content__share-btn-block"> 
                    <a class="product-content__share-btn" href="#"></a> 
                    <ul class="product-content__share-list"> 
                        <li><a class="product-content__share-fb" href="#"></a></li> 
                        <li><a class="product-content__share-tw" href="#"></a></li> 
                        <li><a class="product-content__share-pint" href="#"></a></li> 
                        <li><a class="product-content__share-goo" href="#"></a></li> 
                        <li><a class="product-content__share-red" href="#"></a></li> 
                        <li><a class="product-content__share-mail" href="#"></a></li> 
                        <li><a class="product-content__share-more" href="#"></a></li> 
                    </ul>
                </div> 
                <a class="product-content__buy-btn" href="#" data-action="order" data-showcase="<?php echo $showcase_name; ?>" data-bike-name="<?php echo $post->post_title; ?>">Ready to buy</a>
                <a class="product-content__configure-btn" href="//brassballscycles.com/c-23-air-cleaners.aspx" target="_blank">Configure yours</a>
            </div>
        </li>
        <?php } ?>
    </ul>
<?php } ?>