<?php

function bballs_bikes_each( $template, $before, $after ) {
    if( ! function_exists( $template ) ) {
        return;
    }
    $all_bikes = bballs_all_bikes();
    if( !empty( $all_bikes ) ) {
        echo $before;
        foreach( $all_bikes as $bike_index => $bike ) {
            call_user_func_array( $template , [ $bike, $bike_index ] );
        }
        echo $after;
    }
}


function bballs_bike_slider( $bike ) {
    global $post;
    if( $post->ID === $bike->ID ) {
        return;
    }
    $bike_cover = $bike->navigation_cover;

    if( empty( $bike_cover ) && has_post_thumbnail( $bike ) ) {
        $bike_cover = get_the_post_thumbnail_url( $bike, 'cover' );
    }

    if( is_numeric( $bike_cover ) ) {
        $bike_cover = wp_get_attachment_image_url( $bike_cover, 'cover' );
    }
    ?>
    <div>
        <div class="product-slider__slide-title">
            <h2><?php echo $bike->post_title; ?></h2>
            <span>Something interesting</span>
        </div>
        <a href="<?php echo get_permalink( $bike->ID ); ?>" style="background-image: url(<?php echo $bike_cover; ?>)" class="product-slider__slide-content">
            <div class="product-slider__dark-bg"></div>
        </a>
    </div>
<?php
}

function bballs_bakes_tabs( $bike ) {
?>
    <li><a class="models-tabs__digger js-scroll-nav-item" href="#<?php echo $bike->post_name; ?>"><?php echo $bike->post_title; ?></a></li>
<?php
}

function bbals_side_bikes( $bike ) {
    ?>
    <li><a href="<?php echo get_permalink( $bike->ID ); ?>"><?php echo get_the_post_thumbnail( $bike, 'bike_small', [ 'class' => '']); ?>
        <h6><?php echo get_the_title( $bike->ID ); ?></h6>
        <?php if( $bike->ready_to_buy ) { ?>
        <span>ready-to-buy bikes available</span>
        <?php } ?>
    </a></li>
    <?php
}