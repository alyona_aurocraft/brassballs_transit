<div class="hidden" data-modal data-order-form>
    <div class="popup-bg"></div>
    <div class="popup">
        <span class="close-popup"></span>
        <?php echo do_shortcode('[contact-form-7 id="9" title="Order form"]'); ?>
    </div>
</div>
<div class="hidden" data-modal data-thank-modal>
    <div class="popup-bg"></div>    
    <div class="popup-thank">
        <span class="close-popup"></span>
        <div class="popup__title">
            <h2>YOUR REQUEST HAS BEEN SUCCESSFULLY SUBMITTED!</h2>
            <h4>WE WILL ANSWER AS SOON AS POSSIBLE!</h4>
        </div>
        <?php
        wp_nav_menu([
            'theme_location' => 'social',
            'menu_class' => 'popup-thank__soc-list',
            'container' => false,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>'
        ]);
        ?>
    </div>
</div>