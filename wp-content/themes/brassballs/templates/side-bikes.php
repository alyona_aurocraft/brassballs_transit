<?php
    $bikes = bballs_all_bikes();
?>
<div class="mnu-popup__head">
    <span>Choose a model</span>
</div>
<div class="mnu-popup__head-mob">
    <span>Bikes</span>
</div>
<?php bballs_bikes_each( 'bbals_side_bikes', '<ul>', '</ul>' ); ?>
