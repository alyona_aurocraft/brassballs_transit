<?php
$gallery_images = get_field( 'gallery_images' );
if( !empty( $gallery_images ) ) {
    $gallery_view = $gallery_thumb = '';

    foreach ( $gallery_images as $image ) { 
        $gallery_view .= '<div class="carousel-slide js-img-slide"><img src="'. $image['sizes']['gallery'].'" width="1300" height="740"></div>';
        $gallery_thumb .= '<div class="carousel-slide js-thumb"><img src="'. $image['sizes']['gallery-thumb'] .'" width="90" height="60"></div>';
    }

    $first_gallery_el = reset( $gallery_images );
    $last_gallery_el = end( $gallery_images );

    $gallery_view = '<div class="prev-first"><img src="'. $last_gallery_el['sizes']['gallery'] .'"></div>'. $gallery_view;
    $gallery_view .= '<div class="next-last"><img src="'. $first_gallery_el['sizes']['gallery'] .'"></div>';
    ?>
    <h2 class="section-title">GALLERY</h2>
    <section class="project-carousel">
        <div class="carousel-track">
            <?php echo $gallery_view; ?>
        </div>
        <div class="carousel-thumbs">
            <?php echo $gallery_thumb; ?>
        </div>
    </section>
<?php } ?>