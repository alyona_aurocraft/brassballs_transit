<!--<ul class="all-bikes-tabs filters">
    <li><a href="javascript:void(0);" data-filter="reset" data-filter-conteiner=".product-list">All bikes</a></li>
    <li><a href="#ready-to-buy" data-filter="ready-to-buy" data-filter-value="1" data-filter-conteiner=".product-list">Ready to buy bikes</a></li>
</ul>-->
<?php
    wp_nav_menu([
        'theme_location' => 'filter',
        'menu_class' => 'all-bikes-tabs filters',
        'container' => false,
        'items_wrap' => '<ul class="%2$s">%3$s</ul>'
    ]);
?>