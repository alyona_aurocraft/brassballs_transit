<?php
get_header();

if( have_posts() ) {
global $post; 
?>
    <?php get_template_part('templates/bike', 'filter' ); ?>
    <ul class="product-list">
    <?php while ( have_posts() ) { the_post(); ?>
        <li id="<?php echo $post->post_name; ?>" class="product-list__item product-list__item-digger js-scroll" data-filter-by="ready-to-buy" data-filter-value="<?php echo $post->ready_to_buy; ?>">
            <div class="product-list__item-info">
                <h2><?php the_title(); ?></h2>
                <h4><?php $post->post_excerpt; ?></h4>
                <?php if( $post->ready_to_buy ) { ?>
                    <span>Ready to buy</span>
                <?php } ?>
                <a href="<?php the_permalink(); ?>">Explore</a>
            </div>
            <?php the_post_thumbnail('bike_large', [ 'class' => 'product-list__img']); ?>
        </li>
    <?php } ?>
    </ul>
<?php }
get_footer();