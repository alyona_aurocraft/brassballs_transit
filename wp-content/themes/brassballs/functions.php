<?php

define( 'BBALLS_THEME_URL', get_stylesheet_directory_uri() );
define( 'BBALLS_THEME_PATH', get_stylesheet_directory() );
define( 'BBALLS_INC_PATH', BBALLS_THEME_PATH .'/inc' );
define( 'BBALLS_TEMPLATES_PATH', BBALLS_THEME_PATH .'/templates' );
define( 'BBALLS_ASSETS_PATH', BBALLS_THEME_PATH .'/assets' );

require_once BBALLS_INC_PATH .'/hooks.php';

function bballs_setup() {
    register_nav_menus( array(
        'primary' => __( 'Top Menu',      'brassballs' ),
        'secondary'  => __( 'Bottom menu', 'brassballs' ),
        'social'  => __( 'Social Links Menu', 'brassballs' ),
        'filter'  => __( 'Bike Filter', 'brassballs' ),
    ) );

    add_theme_support( 'post-thumbnails' );
    add_image_size( 'bike_small', 232, 9999 );
    add_image_size( 'bike_large', 832, 9999 );
    add_image_size( 'design_large', 1058, 450, true );
    add_image_size( 'design_small', 515, 413, true );
    add_image_size( 'cover', 720, 697, true );
    add_image_size( 'gallery', 1300, 740, true );
    add_image_size( 'gallery-thumb', 90, 60, true );

    add_theme_support( 'custom-logo', array(
        'height'      => 248,
        'width'       => 248,
        'flex-height' => true,
    ) );

    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );
}

function bballs_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Home Page setup', 'brassballs' ),
        'id'            => 'home-page',
        'description'   => __( 'Customize home page.', 'brassballs' ),
        'before_widget' => '<div id="%1$s" class="section %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="sectoin-title">',
        'after_title'   => '</h2>',
    ) );
}

function bballs_scripts() {
    wp_register_style( 'brassballs-slick', BBALLS_THEME_URL .'/assets/libs/slick/slick.css', [], '1.6.0' );
    wp_register_style( 'brassballs-perfect-scrollbar', BBALLS_THEME_URL .'/assets/libs/perfect-scrollbar.css', [], '0.6.12' );    
    wp_register_style( 'brassballs-styles', BBALLS_THEME_URL .'/assets/css/style.css', [ 'brassballs-slick', 'brassballs-perfect-scrollbar' ], '20160803' );
    wp_enqueue_style( 'brassballs-theme-style', get_stylesheet_uri(), ['brassballs-styles'], '20160805' );

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', [], '1.11.0', true );
    wp_add_inline_script( 'jquery', 'window.jQuery || document.write(\'<script src="'. BBALLS_THEME_URL .'/assets/libs/jquery.js"><\/script>\')' );

    wp_register_script( 'brassballs-parallax', BBALLS_THEME_URL . '/assets/libs/parallax.min.js', ['jquery'], '1.4.2', true );
    wp_register_script( 'brassballs-slick', BBALLS_THEME_URL . '/assets/libs/slick/slick.min.js', ['jquery'], '1.6.0', true );
    wp_register_script( 'brassballs-perfect-scrollbar', BBALLS_THEME_URL . '/assets/libs/perfect-scrollbar.jquery.js', ['jquery'], '0.6.12', true );
    wp_register_script( 'brassballs-plugins', BBALLS_THEME_URL . '/assets/libs/plugins.js', ['jquery'], '20160803', true );

    wp_enqueue_script( 'brassballs-onscreen', BBALLS_THEME_URL . '/assets/libs/jquery.onscreen.min.js', ['jquery'], '0.0.0', true );
    wp_enqueue_script( 'brassballs-scripts', BBALLS_THEME_URL . '/assets/js/scripts.js', ['jquery', 'brassballs-plugins', 'brassballs-perfect-scrollbar', 'brassballs-slick', 'brassballs-parallax' ], '20160803', true );

    if( is_singular( 'bike' ) ) {
        wp_enqueue_script( 'brassballs-bike', BBALLS_THEME_URL . '/assets/js/product.js', ['jquery'], '20160828', true );
    }
}

function bballs_inline_scripts() {
    
    do_action( 'bballs_inline_styles' );
    
    do_action( 'bballs_inline_scripts' );
}

function bballs_body_class( $classes ) {
    $clear_classes = [];

    if( is_home() ) {
        $clear_classes[] = 'main-page';
    }

    if( is_singular('bike') ) {
        $clear_classes[] = 'product-page';
    }

    if( is_post_type_archive('bike') ) {
        $clear_classes[] = 'product-list-page';
    }

    if( is_page_template('template-ready-to-buy.php') ) {
        $clear_classes[] = 'ready-to-buy-page';
    }

    if( is_admin_bar_showing() ) {
        $clear_classes[] = 'is-admin';
    }

    return $clear_classes;
}

function bballs_clear_menu_item_classes( $classes ) {
    $new_classes = ['menu-item'];

    if( isset( $classes[0] ) ) {
        $new_classes[] = $classes[0];
    }

    return $new_classes;
}

function bballs_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'brassballs' ), max( $paged, $page ) );
    }

    return $title;
}

function bballs_menu_link_attr( $attr, $item, $args ) {
    if( $args->theme_location === 'social' ) {
        $class_name = 'footer-soc-icons__';
        if( !empty( $item->classes[0] ) ) {
            $class_name .= $item->classes[0];
        }
        $attr['class'] = empty( $attr['class'] ) ? $class_name : ' '. $class_name;
    }

    return $attr;
}

function bballs_cf4_addition_scripts() {
?>  
<script script type="text/javascript">  
    (function($) {
        var forms = $('.wpcf7-form');

        if( forms.length > 0 ) {
            forms.each( function() {
                var selfForm = $(this),
                    formDataTag = $('[name="_wpcf7_unit_tag"]', selfForm ).val();

                $('#'+ formDataTag ).on( 'wpcf7:mailsent', function( e ) {
                    var theForm = $(this),
                        popup = theForm.closest('[data-modal]'),
                        thankPopup = $('[data-thank-modal]');

                    popup.trigger('close');

                    thankPopup.trigger('open');
                })
            });
        }
    }(jQuery));
</script>
<?php
}

function bballs_form_render_hook() {
    add_action( 'bballs_inline_scripts', 'bballs_cf4_addition_scripts' );
}

function bballs_order_form_hidden_field( $hidden_fiedls ) {
    if( is_singular('bike') || is_page_template('template-ready-to-buy.php') ) {
        $hidden_fiedls['bike-name'] = '';
        $hidden_fiedls['bike-showcase'] = '';
    }
    return $hidden_fiedls;
}

function bballs_presend_hundler( $components, $form ) {
    
    $replace_name = !empty( $_POST['bike-name'] ) ? $_POST['bike-name'] : __( 'No set Bike', 'brassballs' );
    $replace_showcase = !empty( $_POST['bike-showcase'] ) ? $_POST['bike-showcase'] : __( 'No set Showcase', 'brassballs' );

    $components['body'] = preg_replace(
        [ '/\[hidden-bike\]/', '/\[hidden-showcase\]/' ],
        [ $replace_name, $replace_showcase ],
        $components['body']
    );

    return $components;
}

function bballs_all_bikes() {
    return get_posts([
        'post_type' => 'bike',
        'posts_per_page' => -1
    ]);
}

function fetchData( $url ) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);

    $result = curl_exec($ch);
    
    curl_close($ch);

    return $result;
}

function bballs_post_types() {
    // Include post types
    require_once BBALLS_INC_PATH .'/post-types/bike.php';
}


require_once BBALLS_INC_PATH .'/widgets/home-instagram.php';
require_once BBALLS_INC_PATH .'/widgets/home-section.php';
require_once BBALLS_INC_PATH .'/widgets/home-simple-text.php';

function bballs_widget_register() {
    register_widget( 'Home_Section' );
    register_widget( 'Home_Simple_Text' );
    register_widget( 'Home_Instagram' );
}


// Customizer
require_once BBALLS_INC_PATH .'/customizer.php';

// Templates BBALLS_TEMPLATES_PATH
require_once BBALLS_TEMPLATES_PATH .'/commons.php';