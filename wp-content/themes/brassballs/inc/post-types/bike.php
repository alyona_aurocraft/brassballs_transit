<?php

$labels = array(
    'name'                  => _x( 'Bikes', 'Post type general name', 'brassballs' ),
    'singular_name'         => _x( 'Bike', 'Post type singular name', 'brassballs' ),
    'menu_name'             => _x( 'Bikes', 'Admin Menu text', 'brassballs' ),
    'name_admin_bar'        => _x( 'Bike', 'Add New on Toolbar', 'brassballs' ),
    'add_new'               => __( 'Add New', 'brassballs' ),
    'add_new_item'          => __( 'Add New Bike', 'brassballs' ),
    'new_item'              => __( 'New Bike', 'brassballs' ),
    'edit_item'             => __( 'Edit Bike', 'brassballs' ),
    'view_item'             => __( 'View Bike', 'brassballs' ),
    'all_items'             => __( 'All Bikes', 'brassballs' ),
    'search_items'          => __( 'Search Bikes', 'brassballs' ),
    'parent_item_colon'     => __( 'Parent Bikes:', 'brassballs' ),
    'not_found'             => __( 'No bikes found.', 'brassballs' ),
    'not_found_in_trash'    => __( 'No bikes found in Trash.', 'brassballs' ),
    'featured_image'        => _x( 'Bike Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'brassballs' ),
    'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'brassballs' ),
    'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'brassballs' ),
    'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'brassballs' ),
    'archives'              => _x( 'Bike archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'brassballs' ),
    'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'brassballs' ),
    'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'brassballs' ),
    'filter_items_list'     => _x( 'Filter bikes list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'brassballs' ),
    'items_list_navigation' => _x( 'Bikes list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'brassballs' ),
    'items_list'            => _x( 'Bikes list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'brassballs' ),
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'bike' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
);

register_post_type( 'bike', $args );

unset( $labels, $args );