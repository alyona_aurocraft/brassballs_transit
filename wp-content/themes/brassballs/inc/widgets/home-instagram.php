<?php
class Home_Instagram extends WP_Widget {
    public function __construct() {
        $widget_ops = array( 
            'classname' => 'home-instargem',
            'description' => 'Instagram block',
        );
        parent::__construct( 'home_instargem', 'Home Instagram', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'] .'<div class="main-instagram">';
        if ( ! empty( $instance['user_name'] ) ) {
            echo $args['before_title'] . '<a href="//www.instagram.com/'. $instance['user_name'] .'">@'. apply_filters( 'widget_title', $instance['user_name'] ) .'</a>' . $args['after_title'];
        }

        if( !empty( $instance['user_name'] ) && !empty( $instance['token'] ) ) {
            $size = (int) $instance['rows'] * (int) $instance['cols'];
            $instagram_result = fetchData( 'https://api.instagram.com/v1/users/'. $instance['user_name'] .'/media/recent/?access_token='. $instance['token'] .'&count='. $size * 4 );          
            
            $instagram_result = json_decode( $instagram_result ); ?>
            <?php if( !empty($instagram_result->data) ) { ?>
                <ul>
                    <?php foreach ($instagram_result->data as $post) { ?>
                        <div class="main-instagram__dark-bg"></div><img src="<?php echo $post->images->standard_resolution->url; ?>" alt="alt">
                    <?php } ?>
                </ul>
            <?php }
        } ?>
        <?php echo '</div>'. $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        $user_name = !empty( $instance['user_name'] ) ? $instance['user_name'] : '' ;
        $token = !empty( $instance['token'] ) ? $instance['token'] : '' ;


        $rows = !empty( $instance['rows'] ) ? $instance['rows'] : '2' ;
        $cols = !empty( $instance['cols'] ) ? $instance['cols'] : '4' ;
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'user_name' ) ); ?>"><?php _e( 'Instagram User name:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'user_name' ) ); ?>" value="<?php echo esc_attr( $user_name ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'user_name' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'token' ) ); ?>"><?php _e( 'Access Token:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'token' ) ); ?>" value="<?php echo esc_attr( $token ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'token' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'rows' ) ); ?>"><?php _e( 'Rows:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'rows' ) ); ?>" value="<?php echo esc_attr( $rows ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'rows' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'cols' ) ); ?>"><?php _e( 'Cols:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'cols' ) ); ?>" value="<?php echo esc_attr( $cols ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'cols' ) ); ?>" />
        </p>
        <?php
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        $instance = [];

        $instance['user_name'] = !empty( $new_instance['user_name'] ) ? $new_instance['user_name'] : '' ;
        $instance['token'] = !empty( $new_instance['token'] ) ? $new_instance['token'] : '' ;

        $instance['rows'] = !empty( $new_instance['rows'] ) ? $new_instance['rows'] : '2' ;
        $instance['cols'] = !empty( $new_instance['cols'] ) ? $new_instance['cols'] : '4' ;

        return $instance;
    }
}