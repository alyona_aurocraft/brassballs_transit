<?php
class Home_Section extends WP_Widget {
    public function __construct() {
        $widget_ops = array( 
            'classname' => 'home-section',
            'description' => 'Secton width image/video cover',
        );
        parent::__construct( 'home_section', 'Home Section', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $primal_button_text = !empty( $instance['primal_button_text'] ) ? $instance['primal_button_text'] : false;
        $secont_button_text = !empty( $instance['secont_button_text'] ) ? $instance['secont_button_text'] : false;

        $primal_button_target = !empty( $instance['primal_button_external'] ) ? $instance['primal_button_external'] : false;
        $secont_button_target = !empty( $instance['secont_button_external'] ) ? $instance['secont_button_external'] : false;

        $anhor = !empty( $instance['anhor'] ) ? 'id="'. $instance['anhor'] .'"' : '';

        $secondary_in_new_tab = $primal_in_new_tab = '';
        if( $primal_button_target && preg_match('/^(?:http(?:s)?:)?\/\//', $primal_button_target ) ) {
            $primal_in_new_tab = 'target="_blank"';
        }
        if( $secont_button_target && preg_match('/^(?:http(?:s)?:)?\/\//', $secont_button_target) ) {
            $secondary_in_new_tab = 'target="_blank"';
        }

        if( ! $primal_button_target ) {
            $target = !empty( $instance['primal_button_target'] ) ? $instance['primal_button_target'] : false;
            if( is_numeric( $target ) ) {
                $primal_button_target = get_permalink( $target );
            } else if( is_string( $target ) ) {
                list( $taxonomy, $term_slug ) = explode('/', $target );
                $primal_button_target = get_term_link( $term_slug, $taxonomy );
            }
        }

        if( ! $secont_button_target ) {
            $target = !empty( $instance['secont_button_target'] ) ? $instance['secont_button_target'] : false;
            if( is_numeric( $target ) ) {
                $secont_button_target = get_permalink( $target );
            } else if( is_string( $target ) ) {
                list( $taxonomy, $term_slug ) = explode('/', $target );
                $secont_button_target = get_term_link( $term_slug, $taxonomy );
            }
        }
        ?>
        <?php if( !empty( $anhor ) ) { ?>
            <div id="<?php echo $anhor; ?>" style="height: 0px !important; line-height: 0 !important;"></div>
            <div class="<?php echo $instance['anhor']; ?>">
        <?php }?>
        <?php if( !empty( $instance['bg_video'] ) ) { ?>
        <div class="main-story__dark-bg"></div>
        <video class="content__bg" poster="<?php $instance['bg_image'];  ?>" autoplay="autoplay" preload="none" loop muted>
            <source src="<?php echo $instance['bg_video']; ?>" type="video/mp4">
        </video>
        <?php } else if( !empty( $instance['bg_image'] ) ) { ?>
            <img class="content__bg" src="<?php echo $instance['bg_image']; ?>" alt="alt">
        <?php } ?>
        <?php if( !empty( $instance['bg_image_mob'] ) ) { ?>
            <img class="content__bg-mob" src="<?php echo $instance['bg_image_mob']; ?>" alt="alt">
        <?php } ?>
        <div class="content__inner">
            <?php if( !empty( $instance['text'] ) ) { ?>
            <h2><?php echo $instance['text']; ?></h2>
            <?php } ?>
            <?php if( $primal_button_text && $primal_button_target ) { ?>
            <a class="content-button" href="<?php echo $primal_button_target; ?>" <?php echo $primal_in_new_tab; ?>><?php echo $primal_button_text; ?></a>
            <?php } ?>
            <?php if( $secont_button_text && $secont_button_target ) { ?>
            <a class="main-story__read" href="<?php echo $secont_button_target; ?>" <?php echo $primal_in_new_tab; ?>><?php echo $primal_button_text; ?></a>
            <?php } ?>
        </div>
        <?php if( !empty( $anhor ) ) { ?>
            </div>
        <?
        }
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $bg_image = !empty( $instance['bg_image'] ) ? $instance['bg_image'] : '';
        $bg_image_mob = !empty( $instance['bg_image_mob'] ) ? $instance['bg_image_mob'] : '';
        $bg_video = !empty( $instance['bg_video'] ) ? $instance['bg_video'] : '';

        $text = !empty( $instance['text'] ) ? $instance['text'] : '';

        $primal_button_text = !empty( $instance['primal_button_text'] ) ? $instance['primal_button_text'] : '';
        $primal_button_target = !empty( $instance['primal_button_target'] ) ? $instance['primal_button_target'] : '';
        $primal_button_external = !empty( $instance['primal_button_external'] ) ? $instance['primal_button_external'] : '';

        $secont_button_text = !empty( $instance['secont_button_text'] ) ? $instance['secont_button_text'] : '';
        $secont_button_target = !empty( $instance['secont_button_target'] ) ? $instance['secont_button_target'] : '';
        $secont_button_external = !empty( $instance['secont_button_external'] ) ? $instance['secont_button_external'] : '';

        $anhor = !empty( $instance['anhor'] ) ? $instance['anhor'] : '';

        $target_select_options = [];

        $target_select_options['post'] = get_posts([
            'posts_per_page' => '-1'
        ]);
        $target_select_options['page'] = get_posts([
            'post_type' => 'page',
            'posts_per_page' => '-1'
        ]);

        $currents_post_types = get_post_types([
           'public'   => true,
           '_builtin' => false
        ]);
        foreach( $currents_post_types as $post_type ) {
            $target_select_options[ $post_type ] = get_posts([
                'post_type' => $post_type,
                'posts_per_page' => '-1'
            ]);
        }

        $currents_taxonomies = get_taxonomies([
           'public'   => true,
           '_builtin' => true
        ]);
        foreach( $currents_taxonomies as $taxonomy ) {
            $target_select_options[ $taxonomy ] = get_terms([
                'taxonomy' => $taxonomy,
                'hierarchical' => false
            ]);
        }
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bg_image' ) ); ?>"><?php _e( 'Background image', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'bg_image' ) ); ?>" value="<?php echo esc_attr( $bg_image ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bg_image' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bg_image_mob' ) ); ?>"><?php _e( 'Background image for mobile:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'bg_image_mob' ) ); ?>" value="<?php echo esc_attr( $bg_image_mob ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bg_image_mob' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'bg_video' ) ); ?>"><?php _e( 'Video (optional):', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'bg_video' ) ); ?>" value="<?php echo esc_attr( $bg_video ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bg_video' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php _e( 'Text:', 'brassballs' ); ?></label>
            <textarea type="text" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php echo esc_attr( $text ); ?></textarea>
        </p>
        <h4><?php _e( 'Primal button', 'brassballs' ) ?></h4>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'primal_button_text' ) ); ?>"><?php _e( 'Button text:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'primal_button_text' ) ); ?>" value="<?php echo esc_attr( $primal_button_text ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'primal_button_text' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'primal_button_target' ) ); ?>"><?php _e( 'Button target:', 'brassballs' ); ?></label>
            <select name="<?php echo esc_attr( $this->get_field_name( 'primal_button_target' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'primal_button_target' ) ); ?>">
                <option value="-1"><?php _e( 'Select page', 'brassballs' ); ?></option>
                <?php foreach( $target_select_options as $opt_group_type => $opt_group ) {
                    $opt_group_obj = get_taxonomy( $opt_group_type );
                    if( ! $opt_group_obj ) {
                        $opt_group_obj = get_post_type_object( $opt_group_type );
                    }
                    
                    $optgroup_label = !is_null( $opt_group_obj ) ? $opt_group_obj->label : __('General', 'brassballs');

                    echo '<optgroup label="'. $optgroup_label .'">';
                    foreach( $opt_group as $opt ) {
                        if( !empty( $opt->slug ) ) {
                            $opt_value = $opt_group_type.'/'. $opt->slug;
                            $opt_label = $opt->name;
                        } else {
                            $opt_value = $opt->ID;
                            $opt_label = $opt->post_title;
                        }

                        $selected = selected( $primal_button_target, $opt_value, false );
                        echo '<option value="'. $opt_value .'" '. $selected .'>'. $opt_label .'</option>';
                    }
                } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'primal_button_external' ) ); ?>"><?php _e( 'External link (have more priority):', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'primal_button_external' ) ); ?>" value="<?php echo esc_attr( $primal_button_external ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'primal_button_external' ) ); ?>" placeholder="http://" />
        </p>

        <h4><?php _e( 'Second button', 'brassballs' ) ?></h4>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'secont_button_text' ) ); ?>"><?php _e( 'Button text:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'secont_button_text' ) ); ?>" value="<?php echo esc_attr( $secont_button_text ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'secont_button_text' ) ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'secont_button_target' ) ); ?>"><?php _e( 'Button target:', 'brassballs' ); ?></label>
            <select name="<?php echo esc_attr( $this->get_field_name( 'secont_button_target' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'secont_button_target' ) ); ?>">
                <option value="-1"><?php _e( 'Select page', 'brassballs' ); ?></option>
                <?php foreach( $target_select_options as $opt_group_type => $opt_group ) {
                    $opt_group_obj = get_taxonomy( $opt_group_type );
                    if( ! $opt_group_obj ) {
                        $opt_group_obj = get_post_type_object( $opt_group_type );
                    }
                    
                    $optgroup_label = !is_null( $opt_group_obj ) ? $opt_group_obj->label : __('General', 'brassballs');

                    echo '<optgroup label="'. $optgroup_label .'">';
                    foreach( $opt_group as $opt ) {
                        if( !empty( $opt->slug ) ) {
                            $opt_value = $opt_group_type .'/'. $opt->slug;
                            $opt_label = $opt->name;
                        } else {
                            $opt_value = $opt->ID;
                            $opt_label = $opt->post_title;
                        }
                        $selected = selected( $secont_button_target, $opt_value, false );

                        echo '<option value="'. $opt_value .'" '. $selected .'>'. $opt_label .'</option>';
                    }
                } ?>
            </select>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'secont_button_external' ) ); ?>"><?php _e( 'External link (have more priority):', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'secont_button_external' ) ); ?>" value="<?php echo esc_attr( $secont_button_external ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'secont_button_external' ) ); ?>" placeholder="http://" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'anhor' ) ); ?>"><?php _e( 'Anhor:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'anhor' ) ); ?>" value="<?php echo esc_attr( $anhor ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'anhor' ) ); ?>" placeholder="#" />
        </p>
        <?php
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = [];

        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['bg_image'] = !empty( $new_instance['bg_image'] ) ? $new_instance['bg_image'] : '';
        $instance['bg_image_mob'] = !empty( $new_instance['bg_image_mob'] ) ? $new_instance['bg_image_mob'] : '';
        $instance['bg_video'] = !empty( $new_instance['bg_video'] ) ? $new_instance['bg_video'] : '';

        $instance['text'] = !empty( $new_instance['text'] ) ? $new_instance['text'] : '';

        $instance['primal_button_text'] = !empty( $new_instance['primal_button_text'] ) ? $new_instance['primal_button_text'] : '';
        $instance['primal_button_target'] = !empty( $new_instance['primal_button_target'] ) ? $new_instance['primal_button_target'] : '';
        $instance['primal_button_external'] = !empty( $new_instance['primal_button_external'] ) ? $new_instance['primal_button_external'] : '';

        $instance['secont_button_text'] = !empty( $new_instance['secont_button_text'] ) ? $new_instance['secont_button_text'] : '';
        $instance['secont_button_target'] = !empty( $new_instance['secont_button_target'] ) ? $new_instance['secont_button_target'] : '';
        $instance['secont_button_external'] = !empty( $new_instance['secont_button_external'] ) ? $new_instance['secont_button_external'] : '';

        $instance['anhor'] = !empty( $new_instance['anhor'] ) ? $new_instance['anhor'] : '';

        return $instance;
    }
}