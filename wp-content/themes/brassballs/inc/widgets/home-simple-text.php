<?php
class Home_Simple_Text extends WP_Widget {
    public function __construct() {
        $widget_ops = array( 
            'classname' => 'main-info-block',
            'description' => 'Text widget',
        );
        parent::__construct( 'home_text', 'Home Text Widget', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $anhor = !empty( $instance['anhor'] ) ? 'id="'. $instance['anhor'] .'"' : '';

        ?>
        <div class="main-info-block__inner <?php echo $instance['anhor']; ?>" <?php echo $anhor; ?>>
            <?php echo wpautop( $instance['text'] ); ?>
        </div>
        <?php
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $text = !empty( $instance['text'] ) ? $instance['text'] : '';

        $anhor = !empty( $instance['anhor'] ) ? $instance['anhor'] : '';

        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php _e( 'Text:', 'brassballs' ); ?></label>
            <textarea type="text" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php echo esc_attr( $text ); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'anhor' ) ); ?>"><?php _e( 'Anhor:', 'brassballs' ); ?></label>
            <input type="text" name="<?php echo esc_attr( $this->get_field_name( 'anhor' ) ); ?>" value="<?php echo esc_attr( $anhor ); ?>" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'anhor' ) ); ?>" placeholder="#" />
        </p>
        <?php
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        $instance = [];

        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['text'] = !empty( $new_instance['text'] ) ? $new_instance['text'] : '';

        $instance['anhor'] = !empty( $new_instance['anhor'] ) ? $new_instance['anhor'] : '';

        return $instance;
    }
}