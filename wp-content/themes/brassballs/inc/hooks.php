<?php

/**
 * Remove not important elements
 */
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'rsd_link');
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

/**
 * Setup themes
 * location: functions.php
 */
add_action( 'after_setup_theme', 'bballs_setup' );

/**
 * Register widget area
 * location: functions.php
 */
add_action( 'widgets_init', 'bballs_widgets_init' );

/**
 * Register post types
 * location: function.php
 */
add_action( 'init', 'bballs_post_types' );

/**
 * Scripts and styles
 * location: functions.php
 */
add_action( 'wp_enqueue_scripts', 'bballs_scripts' );

/**
 * Clear body classes
 * location: functions.php
 */
add_filter( 'body_class', 'bballs_body_class' );

/**
 * Remove id for menu item
 */
add_filter('nav_menu_item_id', '__return_empty_string');

/**
 * Clear menu items classes
 * location: functions.php
 */
add_filter('nav_menu_css_class', 'bballs_clear_menu_item_classes');

/**
 * Customize page title
 * location: functions.php
 */
add_filter( 'wp_title', 'bballs_wp_title', 10, 2 );

/**
 * Register Widgets
 */
add_action( 'widgets_init', 'bballs_widget_register' );

/**
 * Menu link attributes
 * location: functions.php
 */
add_filter( 'nav_menu_link_attributes', 'bballs_menu_link_attr', 10, 3 );

/**
 * Contact Form 7 add hidden fields for bike page
 * location: functions.php
 */
add_filter( 'wpcf7_form_hidden_fields', 'bballs_order_form_hidden_field' );

/**
 * Contacr form replace hidden fields
 * location: function.php;
 */
add_filter( 'wpcf7_mail_components', 'bballs_presend_hundler', 10, 2 );

/**
 * Add addition resource for form
 * location: functions.php
 */
add_action( 'wpcf7_contact_form', 'bballs_form_render_hook' );

/**
 * Add inline sources in footer
 * location: functions.php
 */
add_action( 'wp_footer', 'bballs_inline_scripts', 99 );